# IBOOKING - React App

React App.

## Development Setup

- Clone Repository: `git clone HTTPS/SSH Url`
- Move to root directory: `cd ibooking`
- Install dependency: `npm install`
- Running command: `npm start`
- Open link in browser: [http://localhost:3000/](http://localhost:3000/)

## Packages Used

- "bootstrap": "^4.5.2",
- "react": "^16.13.1",
- "react-bootstrap": "^1.3.0",
- "react-dom": "^16.13.1",
- "react-hook-form": "^6.8.6",
- "react-router-dom": "^5.2.0",
- "react-slick": "^0.27.11",
- "slick-carousel": "^1.8.1"

## Deploy

- Test Deployment: `npm run test-deploy`

## Testing Server

[StartTesting](http://coinboxtesting.s3-website.ap-south-1.amazonaws.com/)

## Developer Best Practice

- Maintain proper namespacing for folders, files, variable and function declarations.
- Format code using [Prettier](https://www.npmjs.com/package/prettier) package.
- Always create feature or bug branches and then merge with stable master branch.
- Provide proper commit messages & split commits meaningfully.
