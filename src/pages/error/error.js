import React from 'react';
import error from '../../assets/images/error.png';
import './error.css';

const Error = () => {
  return (
    <div className="error-page">
      <div className="error-img">
        <img src={error} alt="error" />
      </div>
    </div>
  );
};

export default Error;
