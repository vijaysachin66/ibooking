import React from 'react';
import './contact.css';
import Headerbg from '../../components/header/headerbg';
import Footer from '../../components/footer/footer';
import { Col, Row, Button } from 'react-bootstrap';
import { useForm } from 'react-hook-form';

const Contact = () => {
  const { register, handleSubmit, watch, errors } = useForm();
  const onSubmit = (data) => console.log(data);
  console.log(watch('example'));

  return (
    <div className="contact-page">
      <Headerbg />
      <div className="contact-form-section common-padding">
        <div className="contact-head">
          <h2>Contact ibooking</h2>
        </div>

        <Row>
          <Col md={6}>
            <div className="contact-form">
              <form onSubmit={handleSubmit(onSubmit)}>
                <div className="name-setion">
                  <div className="form-input">
                    <label>Your Firstname *</label>
                    <input
                      name="firstname"
                      placeholder="Enter your firstname"
                      ref={register({
                        required: true,
                        minLength: 2,
                        pattern: /^[A-Z]+/i
                      })}
                    />
                    <span>
                      {errors?.firstname?.type === 'required' &&
                        'This field is required'}
                      {errors?.firstname?.type === 'pattern' &&
                        'Firstame contain Numbers'}
                    </span>
                  </div>
                  <div className="form-input">
                    <label>Your Lastname *</label>
                    <input
                      name="lastname"
                      placeholder="Enter your lastname"
                      ref={register({ required: true, pattern: /^[A-Z]+/i })}
                    />
                    <span>
                      {errors?.lastname?.type === 'required' &&
                        'This field is required'}
                      {errors?.lastname?.type === 'pattern' &&
                        'Lastname contain Numbers'}
                    </span>
                  </div>
                </div>
                <div className="form-input">
                  <label>Your Email *</label>
                  <input
                    name="email"
                    placeholder="Enter your email"
                    ref={register({
                      required: true,
                      pattern: /^[A-Z0-9._%+-]+@[A-Z.]+\.[A-Z]{2,}$/i
                    })}
                  />
                  <span>
                    {errors?.email?.type === 'required' &&
                      'This field is required'}
                    {errors?.email?.type === 'pattern' && 'Invalid Email Id'}
                  </span>
                </div>
                <div className="form-input">
                  <label>Your Message *</label>
                  <textarea
                    name="message"
                    placeholder="Enter youe message"
                    rows={5}
                    ref={register({ required: true })}
                  />
                  <span>
                    {errors?.message?.type === 'required' &&
                      'This field is required'}
                    {errors?.message?.type === 'pattern' && 'Invalid Email Id'}
                  </span>
                </div>
                <div className="send-btn">
                  <Button type="submit">Send message</Button>
                </div>
              </form>
            </div>
          </Col>
          <Col md={6}>
            <div className="contact-text">
              <div className="contact-text-head">
                <h2>General Enquiries</h2>
                <p>
                  We're here to help! The fastest way to get in touch with the
                  ibooking team is via the online form. Please note that we are
                  not the booking provider.{' '}
                </p>
              </div>
              <div className="contact-text-head">
                <h2>Customer Enquiries</h2>
                <p>
                  We're here to help! The fastest way to get in touch with the
                  ibooking team is via the online form. Please note that we are
                  not the booking provider.{' '}
                </p>
                <span>- Customer Login </span>
              </div>
              <div className="contact-text-head">
                <h2>Business Enquiries</h2>
                <p>
                  We're here to help! The fastest way to get in touch with the
                  ibooking team is via the online form. Please note that we are
                  not the booking provider.{' '}
                </p>
                <span>- Business Login </span>
              </div>
            </div>
          </Col>
        </Row>
      </div>
      <Footer />
    </div>
  );
};

export default Contact;
