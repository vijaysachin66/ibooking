import React from 'react';
import './home.css';
import banner from '../../assets/images/banner.jpg';
import mobile from '../../assets/images/mobile.png';
import playstore from '../../assets/images/playstore.png';
import apple from '../../assets/images/apple.png';
import bussiness from '../../assets/images/bussiness.png';
import arrow from '../../assets/images/arrow-right.png';

import female from '../../assets/images/female.png';
import makeup from '../../assets/images/makeup.png';
import children from '../../assets/images/children.png';
import car from '../../assets/images/car.png';
import rouse from '../../assets/images/rouse.png';
import person from '../../assets/images/person.png';
import map from '../../assets/images/map.png';

import { Col, Row } from 'react-bootstrap';
import FeatureCard from '../../components/featurecard/featurecard';
import PopularCard from '../../components/popularcard/popularcard';
import Header from '../../components/header/header';
import Footer from '../../components/footer/footer';
import Slider from 'react-slick';

const Home = () => {
  var card = {
    dots: false,
    infinite: false,
    arrow: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true
        }
      }
    ]
  };
  return (
    <div className="home-page">
      {/* Banner Section */}
      <div className="banner-section">
        <div className="overlay"></div>
        <img src={banner} alt="banner" />
        <Header />
        <div className="banner-form">
          <h2>Simple, Powerful, Convenient</h2>
          <div className="input-section">
            <div className="custom-input">
              <input placeholder="What would you like to book?" />
            </div>
            <div className="custom-input">
              <input placeholder="Location" />
              <span>
                <img src={map} alt="map" />
              </span>
            </div>
            <div className="search-btn">
              <a href="/">SEARCH</a>
            </div>
          </div>
        </div>
      </div>
      {/* Welcome Section */}
      <div className="welcome-section odd common-padding">
        <div className="welcome-head common-heading">
          <h2>Welcome to ibooking</h2>
          <h3>The booking management system that works for your business</h3>
        </div>
        <div className="welcome-desc">
          <p>
            If you run your own business, you'll know how difficult it is to
            keep on top of all the small - but Important- details. So why not
            make things easier on yourself with ibooking? An innovatiw platform
            designed with businesses in mind. it allows your customers to easily
            browse. secure and manage bookings in just a few clicks - loving you
            free to focus on what you do best And if you're in the market for
            local services in your area. booking is great news.. With our clever
            system, it couldn't be easier to make arrangements with your choice
            of provider.
          </p>
          <p>
            Whether you're a business looking for smart, cloud-based booking
            solutions or a customer searching for the best service, booking has
            got you covered.
          </p>
        </div>
        <div className="popular-service">
          <div className="common-heading">
            <h2>Popular services near you</h2>
          </div>
          <div className="popular-card-section">
            <Slider {...card}>
              <PopularCard />
              <PopularCard />
              <PopularCard />
              <PopularCard />
              <PopularCard />
              <PopularCard />
              <PopularCard />
              <PopularCard />
              <PopularCard />
              <PopularCard />
            </Slider>
          </div>
        </div>
      </div>
      {/* Feature Sections */}
      <div className="feature-section even common-padding">
        <div className="common-heading">
          <h2>Featured businesses near you</h2>
        </div>
        <div className="feature-card-section">
          <Row>
            <Col md={3}>
              <FeatureCard />
            </Col>
            <Col md={3}>
              <FeatureCard />
            </Col>
            <Col md={3}>
              <FeatureCard />
            </Col>
            <Col md={3}>
              <FeatureCard />
            </Col>
          </Row>
        </div>
        <div className="feature-card-section">
          <Row>
            <Col md={3}>
              <FeatureCard />
            </Col>
            <Col md={3}>
              <FeatureCard />
            </Col>
            <Col md={3}>
              <FeatureCard />
            </Col>
            <Col md={3}>
              <FeatureCard />
            </Col>
          </Row>
        </div>
      </div>
      {/* Powerful Sections */}
      <div className="powerful-section odd common-padding">
        <Row>
          <Col md={6}>
            <div className="powerful-section-text">
              <h2>Simple, Powerful Booking System for Your Business</h2>
              <p>
                ibooking is two-sided, So whether you are a customer looking to
                book a service or a business looking for o great (cloud-based)
                booking software.{' '}
              </p>
            </div>
          </Col>
          <Col md={6}>
            <div className="powerful-section-count">
              <div className="powerful-section-count-list">
                <ul>
                  <li>
                    <h2>2,000+</h2>
                    <label>Businesses use ibooking.</label>
                  </li>
                  <li>
                    <h2>1.5M+</h2>
                    <label>Customers hove used ibooking.</label>
                  </li>
                  <li>
                    <h2>Nationwide</h2>
                    <label> We cover oil of UK </label>
                  </li>
                  <li>
                    <h2>It's FREE</h2>
                    <label>All standard features ore free</label>
                  </li>
                </ul>
              </div>
            </div>
          </Col>
        </Row>
      </div>
      {/* Mobile Apps */}
      <div className="mobile-app-section even common-padding">
        <Row>
          <Col md={6}>
            <div className="mobile-app-img">
              <img src={mobile} alt="mobile" />
            </div>
          </Col>
          <Col md={6}>
            <div className="common-heading mobile-app-text-section">
              <h2>Mobile apps coming soon!</h2>
              <p>
                our team are working hard to develop awesome apps for our
                customers as well as our partners. The !booking apps will be
                available in irtS and Android.
              </p>
              <div className="mobile-app-icon-section">
                <div className="mobile-app-icon-list">
                  <div className="mobile-app-icon">
                    <img src={playstore} alt="playstore" />
                  </div>
                  <div className="mobile-app-text">
                    <h3>Get it on the</h3>
                    <label>Google Play</label>
                  </div>
                </div>
                <div className="mobile-app-icon-list">
                  <div className="mobile-app-icon">
                    <img src={apple} alt="playstore" />
                  </div>
                  <div className="mobile-app-text">
                    <h3>Download on the</h3>
                    <label>App store</label>
                  </div>
                </div>
              </div>
            </div>
          </Col>
        </Row>
      </div>
      {/* Software Section */}
      <div className="software-section common-padding">
        <Row>
          <Col md={6}>
            <div className="software-section-text">
              <h2>
                Finally! A booking software that really works for my business
              </h2>
              <p>
                Give your business the perfect tool to grow and become more
                efficient ibooking is designed to make the customer journey and
                the business process much more convenient, allowing you to focus
                on running the business.
              </p>
              <a href="/">Learn more</a>
            </div>
          </Col>
          <Col md={6}>
            <div className="software-section-image">
              <img src={bussiness} alt="bussiness" />
            </div>
          </Col>
        </Row>
      </div>
      {/* Booking Management */}
      <div className="booking-management odd common-padding">
        <Row>
          <Col lg={6}>
            <div className="booking-management-text">
              <h2>Booking management software to suit your business</h2>
              <h3>
                Simple, flexible and powerful booking software for your
                business. and it's totally FREE!
              </h3>
              <div className="booking-software-list">
                <label>Coach & bus hire booking software</label>
                <span>
                  <img src={arrow} alt="arrow" />
                </span>
              </div>
              <div className="booking-software-list">
                <label>Bouncy castle hire booking software</label>
                <span>
                  <img src={arrow} alt="arrow" />
                </span>
              </div>
              <div className="booking-software-list">
                <label> Mobile hair & makeup artist booking software</label>
                <span>
                  <img src={arrow} alt="arrow" />
                </span>
              </div>
              <div className="booking-software-list-notes">
                <p>
                  Learn more about our cloud based booking management software
                </p>
              </div>
            </div>
          </Col>
          <Col lg={6}>
            <div className="booking-management-image">
              <div className="booking-management-image-first">
                <img src={person} alt="arrow" />
                <img src={rouse} alt="arrow" />
                <img src={makeup} alt="arrow" />
              </div>
              <div className="booking-management-image-second">
                <img src={car} alt="arrow" />
                <img src={female} alt="arrow" />
                <img src={children} alt="arrow" />
              </div>
            </div>
          </Col>
        </Row>
      </div>
      <Footer />
    </div>
  );
};

export default Home;
