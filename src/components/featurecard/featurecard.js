import React from 'react';
import img from '../../assets/images/banner.jpg';
import './featurecard.css';

const FeatureCard = () => {
  return (
    <div className="feature-card">
      <div className="feature-card-image">
        <img src={img} alt="banner" />
      </div>
      <div className="feature-card-content">
        <h2>Modern, Well-Appointed Room </h2>
        <div className="star">
          <span className="fa fa-star checked"></span>
          <span className="fa fa-star checked"></span>
          <span className="fa fa-star checked"></span>
          <span className="fa fa-star checked"></span>
          <span className="fa fa-star checked"></span>
        </div>
        <p>36b Talbot green, Liantrisant,Wales, CF728AF </p>
      </div>
    </div>
  );
};

export default FeatureCard;
