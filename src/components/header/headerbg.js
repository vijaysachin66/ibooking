import React, { useState } from 'react';
import './header.css';
import logo from '../../assets/images/logo.png';
import logoblack from '../../assets/images/logoblack.png';
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Headerbg = () => {
  const [isHide] = useState(true);
  const [isTop] = useState(true);

  return (
    <div className="header">
      <Navbar
        fixed="top"
        collapseOnSelect
        expand="md"
        className={isTop && 'header-color'}
      >
        <Navbar.Brand href="/">
          {!isHide && (
            <Link to={`/`}>
              <div className="header-logo">
                <img src={logo} alt="logo" />
              </div>
            </Link>
          )}
          {isHide && (
            <Link to={`/`}>
              <div className="header-logo">
                <img src={logoblack} alt="logo" />
              </div>
            </Link>
          )}
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto"></Nav>
          <Nav>
            <Nav.Link>
              <Link to={`/contact`}>Contact</Link>
            </Nav.Link>
            <Nav.Link>
              <Link to={`/login`}>Login</Link>
            </Nav.Link>
            <Nav.Link>
              <ul className="hamburger">
                <i className="fa fa-bars"></i>
              </ul>
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </div>
  );
};

export default Headerbg;
