import React from 'react';
import img from '../../assets/images/banner.jpg';
import './popularcard.css';

const PopularCard = () => {
  return (
    <div className="popular-card">
      <div className="popular-card-image">
        <img src={img} alt="banner" />
      </div>
      <div className="popular-card-content">
        <h2>Business Name </h2>
      </div>
    </div>
  );
};

export default PopularCard;
