import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

// Application Components.
import Home from '../pages/home/home';
import Contact from '../pages/contact/contact';
import Error from '../pages/error/error';

const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route exact path="/contact">
          <Contact />
        </Route>
        <Route component={Error} />
      </Switch>
    </Router>
  );
};

export default Routes;
